//
//  EditView.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditView : UIView

@property (nonatomic, strong) UIButton *iconButton;

@property (nonatomic, strong) UITextField *titleTextField;
@property (nonatomic, strong) UITextView *detailTextView;

@property (nonatomic, assign) CGFloat topOffset;

@end
